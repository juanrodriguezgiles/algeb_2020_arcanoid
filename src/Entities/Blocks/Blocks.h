#ifndef BLOCKS_H
#define BLOCKS_H
#include "raylib.h"
namespace game
{
	struct Blocks {
		Rectangle properties;
		Color color;
		bool active;
	};
	namespace blocks {
		extern const short maxblocks;
		extern Blocks blocksLevel[];
		void setPropertiesBlocks(const int screenWidth);
		void drawBlocks();
	}
}
#endif // !BLOCKS_H