#include "Blocks.h"
namespace game
{
	namespace blocks {
		static short blocksIndex;
		static const short rows = 5;
		static const short columns = 13;
		static const short blockWidht = 42;
		static const short blockHeight = 20;
		static const short initialPosY = 5;
		const short blocksMax = rows * columns;
		const short maxblocks = blocksMax;
		short aux_blocks = 0;
		Blocks blocksLevel[blocksMax];

		void setPropertiesBlocks(const int screenWidth) {
			Color colors[rows]{ RED,YELLOW,BLUE,PURPLE,GREEN };
			blocksIndex = 0;

			for (short i = 0; i < rows; i++)
			{
				for (short j = 0; j < columns; j++)
				{
					blocksLevel[blocksIndex].color = colors[i];

					blocksLevel[blocksIndex].active = true;

					blocksLevel[blocksIndex].properties.width = blockWidht;
					blocksLevel[blocksIndex].properties.height = blockHeight;

					blocksLevel[blocksIndex].properties.x = j * blocksLevel[blocksIndex].properties.width + 4 * j;
					blocksLevel[blocksIndex].properties.y = i * blocksLevel[blocksIndex].properties.height +initialPosY+ 5 * i;

					blocksIndex++;
				}
			}
		}
		void drawBlocks() {
			for (short i = 0; i < blocksMax; i++) {
				if (blocksLevel[i].active)
					DrawRectangleRec(blocksLevel[i].properties, blocksLevel[i].color);
			}
		}
	}
}