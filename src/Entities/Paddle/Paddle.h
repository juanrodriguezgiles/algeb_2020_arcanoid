#ifndef PADDLE_H
#define PADDLE_H
#include "raylib.h"
namespace game
{
	struct KeyBindings
	{
		int left = KEY_A;
		int right = KEY_D;
	};
	struct Paddle
	{
		Rectangle body;
		Color color;
		KeyBindings controls;
		int speed;
		int maxSpeed;
		short lifes;
	};
	namespace paddle
	{
		extern Paddle paddle;

		void GeneratePaddle();
		void MovePaddle();
		void ShowPaddle();
	}
}
#endif // !PADDLE_H