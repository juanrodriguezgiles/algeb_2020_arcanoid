#pragma once
#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "raylib.h"
#include "../../Entities/Paddle/Paddle.h"
#include "../../Entities/Ball/Ball.h"
#include "../../Entities/Blocks/Blocks.h"
#include "../../Game/Game.h"
namespace game
{
	namespace gameplay
	{	
		void init();
		void update();
		void draw();
	}
}
#endif // !GAMEPLAY_H
