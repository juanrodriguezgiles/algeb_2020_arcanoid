#include "Gameplay.h"
#include "../../Game/Game.h"

namespace game {
	namespace gameplay {
		void init()
		{
			game::paddle::GeneratePaddle();
			game::blocks::setPropertiesBlocks(game::screenWidth);
			game::ball::generateBall();
		}
		void update()
		{
			if (!game::ball::ball.launched)
			{
				game::ball::checkLaunch();
			}
			else
			{
				game::ball::moveBall();
			}
			if (IsKeyPressed(KEY_P)) {
				pause_menu::Init(true);
				gameStatus = GAME_STATUS::PAUSE;
			}
			game::paddle::MovePaddle();
			game::ball::checkCollisionBlocks();
			game::ball::checkCollisionPaddle();
			game::ball::checkCollisionWalls();
		}
		void draw()
		{
			game::paddle::ShowPaddle();
			game::ball::drawBall();
			game::blocks::drawBlocks();
		}
	}
}