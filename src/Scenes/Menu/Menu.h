#ifndef MENU_H
#define MENU_H
#include "raylib.h"
namespace game
{
	namespace menu
	{
		enum class MAIN_MENU
		{
			PLAY = 1,
			EXIT
		};
		void Init();
		void Update();
		void Draw();
		bool CheckOptionMainMenu(int auxOption);
		int GetOptionInput();
		void MoveOption();
		bool AcceptOption();
		Color GetColorOptionMenu(int auxOption);
	}
}
#endif // !MENU_H